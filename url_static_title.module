<?php

/**
 * Implements hook_field_formatter_info().
 */
function url_static_title_field_formatter_info() {
  return array(
    'url_static_title_formatter' => array( //Machine name of the formatter
      'label' => t('Static Title'),
      'field types' => array('url'),
      //This will only be available to url fields
      'settings' => array(
        'static_title' => 'Static Title',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function url_static_title_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  // Get the view_mode where our settings are stored
  $display = $instance['display'][$view_mode];
  // Gets the current settings
  $settings = $display['settings'];

  $element = array();

  $element['static_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Static Title'),
    '#description' => t('Static title to use for this URL field. Overrides other titles.'),
    '#default_value' => $settings['static_title'],
  );
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function url_static_title_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = t('Use a static title of "@static_title"', array(
    '@static_title' => $settings['static_title'],
  ));

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function  url_static_title_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $settings = $display['settings'];
  $static_title = $settings['static_title']; // The title assigned in settings

  // Of course having the same title for multiple links will be of questionable use, but if we have multiple fields...
  foreach ($items as $delta => $item) {

    $element[$delta] = array(
      '#type' => 'link',
      '#title' => $static_title,
      '#href' => $item['path'],
      '#options' => $item['options'],
    );
  }

  return $element;
}
